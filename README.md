# Asimov

PHP full stack framework heavily based on the symfony2 components.

# Goals

The idea is to have a basic framework for rapid application development, but with a solid architecture.

This is barely the beginning here, but here are the ideas I'd like to see implemented here, and the components I'd like to use. 

- multi environment (dev/prod/test/console)
- routing, configuration, templating engine (twig, most likely), dependency injection, caching based on SF2 components
- 1 class∕controller action, instead on 1 method/action
- Console. I'm working a lot with CRON jobs, so I need to easily access the services from there, get config data, have reports and so on.
- Lighter 'ORM' than doctrine. I just need to select/update data, i'm not interested in complex engine that can handle many-to-many relationships for me, at least right now. However, I wanna be able to cache stuff (redis), because there are a lot of fun stuff I'd like to do and never took time to implement

# Installation

	php composer.phar install
	mkdir app/cache && chmod u+wx app/cache -R

Then visit web/index_dev.php/hello/plop or web/index_dev.php/stuff
That's pretty much it right now

#Folder architecture

	asimov/
	    Everything related to the Asimov framework
	app/
		The base directory for the application, contains the routes, base configuration, and so on
	src/
		The source for the different modules of the application. 1 module = many controllers, even though right now the only module is not in a specific directory
	vendor/
		External libs
	web/
		The emerged part of the iceberg that you'll expose to the web
