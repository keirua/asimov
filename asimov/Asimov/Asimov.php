<?php
namespace Asimov;

use Symfony\Component\Routing\Matcher\UrlMatcherInterface,
    Symfony\Component\HttpKernel\Controller\ControllerResolverInterface,
    Symfony\Component\EventDispatcher\EventDispatcher,
    Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpKernel\HttpKernelInterface,
    Symfony\Component\Routing;

class Asimov implements HttpKernelInterface {
    protected $matcher;
    protected $resolver;
    protected $dispatcher;

    public function __construct (
            UrlMatcherInterface $matcher,
            ControllerResolverInterface $resolver,
            EventDispatcher $dispatcher)
    {
        $this->matcher = $matcher;
        $this->resolver =   $resolver;
        $this->dispatcher = $dispatcher;
    }
    
    public function handle (Request $request, $type = HttpKernelInterface::MASTER_REQUEST, $catch = true) {
        try {
            $request->attributes->add($this->matcher->match($request->getPathInfo()));
         
            $controller = $this->resolver->getController($request);
            $arguments = $this->resolver->getArguments($request, $controller);
         
            $response = call_user_func_array($controller, $arguments);
        } catch (Routing\Exception\ResourceNotFoundException $e) {
            $response = new Response('Not Found', 404);
        } catch (Exception $e) {
            $response = new Response('An error occurred '.var_dump($e), 500);
        }
        
        $this->dispatcher->dispatch('response', new Event\ResponseEvent($response, $request));

        return $response;
    }
}