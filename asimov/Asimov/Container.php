<?php

namespace Asimov;

use Symfony\Component\DependencyInjection;
use Symfony\Component\DependencyInjection\Reference;

/*
	use Symfony\Component\DependencyInjection\ContainerBuilder;
	use Symfony\Component\Config\FileLocator;
	use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

	$container = new ContainerBuilder();
	$loader = new YamlFileLoader($container, new FileLocator(__DIR__));
	$loader->load('services.yml');
*/

class Container
{
	static public function getContainer (){
		$asimovDIC = new DependencyInjection\ContainerBuilder();

		$router = new Router (__DIR__.'/../../app/routes.yml');
		$routes = $router->getRoutes();

		$asimovDIC->register('asimov.context', 'Symfony\Component\Routing\RequestContext');
		$asimovDIC->register('asimov.urlMatcher', 'Symfony\Component\Routing\Matcher\UrlMatcher')
		    ->setArguments(array($routes, new Reference('asimov.context')));
		$asimovDIC->register('asimov.controllerResolver', 'Asimov\CustomControllerResolver');

		$asimovDIC->register('listener.router', 'Symfony\Component\HttpKernel\EventListener\RouterListener')
		    ->setArguments(array(new Reference('asimov.urlMatcher')));
		$asimovDIC->register('listener.response', 'Symfony\Component\HttpKernel\EventListener\ResponseListener')
		    ->setArguments(array('UTF-8'));
		$asimovDIC->register('listener.exception', 'Symfony\Component\HttpKernel\EventListener\ExceptionListener')
		    ->setArguments(array('Calendar\\Controller\\ErrorController::exceptionAction'));

		$asimovDIC->register('asimov.dispatcher', 'Symfony\Component\EventDispatcher\EventDispatcher')
		    ->addMethodCall('addSubscriber', array(new Reference('listener.router')))
		    ->addMethodCall('addSubscriber', array(new Reference('listener.response')))
		    ->addMethodCall('addSubscriber', array(new Reference('listener.exception')));

		$asimovDIC->register('asimov.framework', 'Asimov\Asimov')
		    ->setArguments(array(new Reference('asimov.urlMatcher'), new Reference('asimov.controllerResolver'), new Reference('asimov.dispatcher')));

		return $asimovDIC;
	}
}
