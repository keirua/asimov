<?php

namespace Asimov;

use Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\Response,
    Symfony\Component\Routing,
    Symfony\Component\HttpKernel;
 

class CustomControllerResolver implements HttpKernel\Controller\ControllerResolverInterface {
    public function getController(Request $request) {
        if (!$controller = $request->attributes->get('_controller')) {
            if (null !== $this->logger) {
                $this->logger->warn('Unable to look for the controller as the "_controller" parameter is missing');
            }
            return false;
        }

        $method = 'run';
        if (!class_exists($controller)) {
            throw new \InvalidArgumentException(sprintf('Class "%s" does not exist.', $controller));
        }

        if (!method_exists($controller, $method)) {
            throw new \InvalidArgumentException(sprintf('Method "%s::%s" does not exist.', $controller, $method));
        }

        return array(new $controller(), $method);
    }

   
    public function getArguments(Request $request, $controller) {
        $r = new \ReflectionMethod($controller[0], $controller[1]);
    
        return $this->doGetArguments($request, $controller, $r->getParameters());
    }

    private function doGetArguments(Request $request, $controller, array $parameters)
    {
        $attributes = $request->attributes->all();
        $arguments = array();
        foreach ($parameters as $param) {
            if (array_key_exists($param->name, $attributes)) {
                $arguments[] = $attributes[$param->name];
            } elseif ($param->getClass() && $param->getClass()->isInstance($request)) {
                $arguments[] = $request;
            } elseif ($param->isDefaultValueAvailable()) {
                $arguments[] = $param->getDefaultValue();
            } else {
                $repr = sprintf('%s::%s()', get_class($controller[0]), $controller[1]);
                throw new \RuntimeException(sprintf('Controller "%s" requires that you provide a value for the "$%s" argument (because there is no default value or because there is a non optional argument after this one).', $repr, $param->name));
            }
        }

        return $arguments;
    }
}