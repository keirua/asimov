<?php
 
// example.com/src/Simplex/ResponseEvent.php
 
namespace Asimov\Event;
 
use Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\Response,
    Symfony\Component\EventDispatcher\Event;
 
class ResponseEvent extends Event
{
    private $request;
    private $response;
 
    public function __construct(Response $response, Request $request)
    {
        $this->response = $response;
        $this->request = $request;
    }
 
    public function getResponse()
    {
        return $this->response;
    }
 
    public function getRequest()
    {
        return $this->request;
    }
}