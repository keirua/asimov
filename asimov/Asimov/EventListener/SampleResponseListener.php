<?php

namespace Asimov\EventListener;
 
use Symfony\Component\EventDispatcher\EventSubscriberInterface,
	Asimov\Event\ResponseEvent;


class SampleResponseListener implements EventSubscriberInterface
{
	public function onResponse (ResponseEvent $event){
		// do nothing
		// die ("blah");
	}

    public static function getSubscribedEvents()
    {
    	// Listen to the 'response' event, and trigger the 'onResponse' method when it happens
        return array('response' => 'onResponse');
    }
} 