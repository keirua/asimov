<?php
namespace Asimov;

use Symfony\Component\Routing;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\Routing\Loader\YamlFileLoader;

class Router
{
	protected $routes;

	public function __construct ($file) {

		$this->routes = new Routing\RouteCollection();
		$loader     = new YamlFileLoader(new FileLocator(__DIR__ . '/'));
		$collection = $loader->load($file);
		$this->routes->addCollection($collection);
	}	
	public function getRoutes (){
		return $this->routes;
	}
}
