<?php


namespace Controller;

use	Symfony\Component\HttpFoundation\Response;



class IndexController {
	public function run ($name){
		$r = new Response('Hello '.$name.' '.rand());

		// cache this response for 10 seconds
		// $r->setTtl (10);

		return $r;
	}
}