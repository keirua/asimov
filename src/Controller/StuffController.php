<?php
namespace Controller;
use	Symfony\Component\HttpFoundation\Response;

class StuffController {
	public function run (){
		ob_start();
	    include sprintf(__DIR__.'/../pages/stuff.php');
	    return new Response(ob_get_clean());
	}
}