<?php

require_once __DIR__.'/../vendor/autoload.php';
use Symfony\Component\HttpFoundation\Request;

$request = Request::createFromGlobals ();

$dic = Asimov\Container::getContainer();
 
$response = $dic->get('asimov.framework')->handle($request);
 
$response->send();